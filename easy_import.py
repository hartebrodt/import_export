import numpy as np

from import_export.import_data import CustomDataImporter


def easy_import(data, header=None, rownames=None, sep='\t', center=False, scale_var=False, scale01=False,
                scale_unit=False, drop_samples=[], log=False, outfile=None, transpose=False):
    importer = CustomDataImporter()
    # if data is a string, it is the filename and has to be imported
    if isinstance(data, str):
        data, sample_ids, variable_names = importer.data_import(data, header=header, rownames=rownames, outfile=outfile,sep=sep, transpose=transpose)
    else:
        sample_ids = None
        variable_names = None
    # remove outlier samples, previously identified
    if len(drop_samples) != 0:
        data = np.delete(data, drop_samples, 0)
    # drop columns with 0 variance or add some pseudocounts with low variance
    data, varn = importer.drop0Columns(data, None, noise=True, drop=False)
    # log(N+1) the data
    if log:
        data = importer.log_transform(data)
    # scale data
    data = importer.scale_data(data, center, scale_var, scale01, scale_unit)
    if outfile:
        importer.export_clean_data(data, sample_ids, variable_names, outfile)
    return data
