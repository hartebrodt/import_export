from setuptools import setup
from Cython.Build import cythonize

setup(
    name='GWAS scaled',
    ext_modules=cythonize("gwas_import.pyx"),
    zip_safe=False,
)